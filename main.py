import vk_api
import numpy as np
from datetime import datetime
import pickle
import requests
import json
import psycopg2
from psycopg2.extras import RealDictCursor


def two_factor():
    code = input('Code? ')
    return code, '4'


vk_session = vk_api.VkApi('+', ':)', auth_handler=two_factor)
vk_session.auth()
vk = vk_session.get_api()
groups = ['secretnsu', 'nsu_enot']

users_groups = []
groups_names = vk.groups.getById(group_ids=groups)

for i, group in enumerate(groups):
    id_users_in_group = []
    offset = 0
    batch = vk.groups.getMembers(group_id=group)['items']
    while len(batch) != 0:
        id_users_in_group.append(batch)
        print(batch[:10])
        offset += len(batch)
        batch = vk.groups.getMembers(group_id=group, offset=offset)['items']
    users_groups.append(set(np.concatenate(id_users_in_group)))

intersected_users = list(users_groups[0].intersection(users_groups[1]))

ages = []

for start_idx in range(0, len(intersected_users), 1000):
    end = min(len(intersected_users), start_idx + 1000)
    users = vk.users.get(user_ids=intersected_users[start_idx:end], fields=['bdate'])
    for user in users:
        if user.get('bdate') is None:
            continue
        else:
            try:
                ages.append(datetime.now().year - int(user['bdate'][-4:]))
            except:
                continue


pickle.dump((intersected_users, np.mean(ages)), open('users.pkl', 'wb'))
print(np.mean(ages))




